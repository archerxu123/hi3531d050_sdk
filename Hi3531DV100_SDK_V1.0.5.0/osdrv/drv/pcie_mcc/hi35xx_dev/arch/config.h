#ifndef __PCI_MCC_HI35XX_DEV__CONGIG_HEADER__
#define __PCI_MCC_HI35XX_DEV__CONGIG_HEADER__

#ifdef CONFIG_PCI  /* For HOST */
	#define __IS_PCI_HOST__			1
	/* #### Hi3531 #### */
	#ifdef CONFIG_ARCH_GODNET
		#define HOST_ARCH_HI3531
		#define HOST_PCIE0_ENABLED	1
		#define HOST_PCIE1_ENABLED	1
	#endif

	/* #### Hi3535 #### */
	#ifdef CONFIG_ARCH_HI3535
		#define HOST_ARCH_HI3535
		#define HOST_PCIE0_ENABLED	1
	#endif

	/* #### Hi3536 #### */
	#ifdef CONFIG_ARCH_HI3536
		#define HOST_ARCH_HI3536
		#define HOST_PCIE0_ENABLED	1
	#endif
	
	/* #### Hi3531A #### */
	#ifdef CONFIG_ARCH_HI3531A
		#define HOST_ARCH_HI3531A
		#define HOST_PCIE0_ENABLED	1
		#define HOST_PCIE1_ENABLED	1

	#endif

	/* #### Hi3531d #### */
	#ifdef CONFIG_ARCH_HI3531D
		#define HOST_ARCH_HI3531D
		#define HOST_PCIE0_ENABLED	1
		#define HOST_PCIE1_ENABLED	1
	#endif
	
	/* #### Hi3519 #### */
	#ifdef CONFIG_ARCH_HI3519
		#define HOST_ARCH_HI3519
		#define HOST_PCIE0_ENABLED	1
	#endif

	/* Macro defined according to slave chip*/
	#if defined(SLV_ARCH_HI3535) \
		|| defined(SLV_ARCH_HI3536) \
		|| defined(SLV_ARCH_HI3519)
			#define SHARED_MEM_IN_SLAVE
	#endif
	#ifdef SLV_ARCH_HI3531A
		#define SHARED_MEM_IN_SLAVE

		/*0:slave hi3531a select pcie 0;
 		 *1:slave hi3531a select pcie 1
 		 *notes:
 		 *if SLV_PCIE0_ENABLED == 1 &&
		     SLV_PCIE1_ENABLED == 0, this value must be 0;
 		 *if SLV_PCIE0_ENABLED == 0 &&
		     SLV_PCIE1_ENABLED == 1, this value must be 1*/
		#define SLV_HI3531A_PCIE_SEL	1
	#endif

	#ifdef SLV_ARCH_HI3531D
		#define SHARED_MEM_IN_SLAVE

		/*0:slave hi3531a select pcie 0;
 		*1:slave hi3531a select pcie 1
		 *notes:
		 *if SLV_PCIE0_ENABLED == 1 && SLV_PCIE1_ENABLED == 0, this value must be 0;
 		*if SLV_PCIE0_ENABLED == 0 && SLV_PCIE1_ENABLED == 1, this value must be 1*/
		#define SLV_HI3531D_PCIE_SEL	1
	#endif

#else /* If not defined CONFIG_PCI, For SLAVE */

	/* #### Hi3531 #### */
	#ifdef CONFIG_ARCH_GODNET
		#define SLV_ARCH_HI3531		1
		/*
		 * Only one PCIe could be enabled!
		 * ---[0:disabled, 1: enabled]
		 */
		/*#define SLV_PCIE0_ENABLED	1*/
		#define SLV_PCIE1_ENABLED	1
	#endif

	/* #### Hi3532 #### */
	#ifdef CONFIG_ARCH_GODCUBE
		#define SLV_ARCH_HI3532		1
		#define SLV_PCIE0_ENABLED	1
	#endif

	/* #### Hi3535 #### */
	#ifdef CONFIG_ARCH_HI3535
		#define SLV_ARCH_HI3535		1
		#define SLV_PCIE0_ENABLED	1
		#define SHARED_MEM_IN_SLAVE
	#endif

	/* #### Hi3536 #### */
	#ifdef CONFIG_ARCH_HI3536
		#define SLV_ARCH_HI3536		1
		#define SLV_PCIE0_ENABLED       1
		#define SHARED_MEM_IN_SLAVE
	#endif

	
	/* #### Hi3531A #### */
	#ifdef CONFIG_ARCH_HI3531A
		#define SLV_ARCH_HI3531A	1
		/*
		 * Only one PCIe could be enabled!
		 * ---[0:disabled, 1: enabled]
		 */
		#define SLV_PCIE0_ENABLED	0
		#define SLV_PCIE1_ENABLED	1
		#define SHARED_MEM_IN_SLAVE
	#endif

	/* #### Hi3519 #### */
	#ifdef CONFIG_ARCH_HI3519
		#define SLV_ARCH_HI3519		1
		#define SLV_PCIE0_ENABLED       1
		#define SHARED_MEM_IN_SLAVE
	#endif
	/* #### Hi3531D #### */
	#ifdef CONFIG_ARCH_HI3531D
		#define SLV_ARCH_HI3531D	1
		/*
		 * Only one PCIe could be enabled!
		 * ---[0:disabled, 1: enabled]
		 */
		#define SLV_PCIE0_ENABLED	0
		#define SLV_PCIE1_ENABLED	1
		#define SHARED_MEM_IN_SLAVE
	#endif
#endif /* CONFIG_PCI */

#endif /* __PCI_MCC_HI35XX_DEV__CONGIG_HEADER__ */

