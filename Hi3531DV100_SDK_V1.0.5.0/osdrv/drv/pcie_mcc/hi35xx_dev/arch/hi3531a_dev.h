
#ifndef __PCIE_MCC_ARCH_HI3531A_HEADER__
#define __PCIE_MCC_ARCH_HI3531A_HEADER__

#define GLOBAL_SOFT_IRQ			83

#define PCIE0_WIN0_BASE			0x30000000
#define PCIE0_WIN0_SIZE			0x100000
#define PCIE0_WIN1_BASE			0x31000000
#define PCIE0_WIN1_SIZE			0x10000
#define PCIE0_WIN2_BASE			0x32000000
#define PCIE0_WIN2_SIZE			0x100000

#define PCIE1_WIN0_BASE			0x60000000
#define PCIE1_WIN0_SIZE			0x1000000
#define PCIE1_WIN1_BASE			0x61000000
#define PCIE1_WIN1_SIZE			0x10000
#define PCIE1_WIN2_BASE			0x62000000
#define PCIE1_WIN2_SIZE			0x100000

#define MISC_CTR_BASE			0x12120000
#define MISC_PCIE0_CTRL_7		0xBC
#define MISC_PCIE1_CTRL_7		0x100

#define SYS_CTRL_BASE			0x12050000

/*
 ************************************************
 **** Base address: PCIe configuration space ****
 */

/* Set 1 to trigger an interrupt[slave] */
#define GLB_SOFT_IRQ_CMD_REG		0x1C

/* Self-defined interrupt status[slave] */
#define HIIRQ_STATUS_REG_OFFSET		0x158

/*
 * Host will mark this reg to tell slave
 * the right RC slave is connecting to
 */
#define DEV_CONTROLLER_REG_OFFSET	0x15C

/*
 * Host will mark this reg to tell slave
 * the right slot_index of the slave
 */
#define DEV_INDEX_REG_OFFSET		0x154
#define SLAVE_SHAKED_BIT		0x6

#if 0
/* System control register for PCIe0 */
#define PERIPHCTRL30			0x/*0xAC*/
/* System control register for PCIe1 */
#define PERIPHCTRL77			0x/*0x168*/
#endif

/* Set this bit to trigger a pcie interrupt */
#define PCIE_INT_X_BIT			(1<<5)


#endif
