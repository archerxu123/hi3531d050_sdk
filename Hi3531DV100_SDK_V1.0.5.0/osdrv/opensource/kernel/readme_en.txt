I) Brief description of the Hi3531DV100 patch applied to v3.18.20 from the linux kernel.
1) in the linux open source community to download v3.18.20 version of the kernel:
	1) Go to the website: www.kernel.org
	2) Select HTTP protocol resources https://www.kernel.org/pub/ option,
	enter the sub-page
	3) Select linux/ menu item, enter the sub-page
	4) Select the kernel/ menu item, enter the sub-page
	5) Select v3.x/ menu item, enter sub-page
	6) Download linux-3.18.20.tar.gz (or linux-3.18.20.tar.xz)

2) patching
	1) The linux-3.18.20.tar.gz stored to hi3531DV100 svn osdrv the directory 
	opensource/kernel
	2) Into the root directory hi3531dv100 svn osdrv In the linux server,
	execute the following command:
		cd opensource/kernel
		tar -zxf linux-3.18.20.tar.gz
		mv linux-3.18.20 linux-3.18.y
		cd linux-3.18.y
		patch -p1 < ../hi3531dv100_for_linux_v3.18.y.patch
		cd ../
		cd ../../

II) The difference between current version and last version is listed in diff_hi3531dv100_from_last_version.patch 
