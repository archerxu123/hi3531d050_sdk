一、将Hi3531DV100的补丁打到v3.18.20的linux kernel上。
1、从linux开源社区下载v3.18.20版本的内核：
        1)进入网站：www.kernel.org
        2)选择HTTP协议资源的https://www.kernel.org/pub/选项,进入子页面
        3)选择linux/菜单项，进入子页面
        4)选择kernel/菜单项，进入子页面
        5)选择v3.x/菜单项，进入子页面
        6)下载linux-3.18.20.tar.gz（或者linux-3.18.20.tar.xz）

2、打补丁
        1)将下载的 linux-3.18.20.tar.gz 存放到 hi3531dv100 svn osdrv的opensource/kernel目录中
		2)在linux服务器中进入 hi3531dv100 svn osdrv 的根目录,执行如下命令：
		cd opensource/kernel
		tar -zxf linux-3.18.20.tar.gz
		mv linux-3.18.20 linux-3.18.y
		cd linux-3.18.y
		patch -p1 < ../hi3531dv100_for_linux_v3.18.y.patch
		cd ../
		cd ../../

二、差异补丁说明
diff_hi3531dv100_from_last_version.patch是当前版本跟上一个版本的差异补丁，主要用于罗列当前版本的改动点。
