/******************************************************************************
  A simple program of Hisilicon Hi35xx video input and output implementation.
  Copyright (C), 2014-2015, Hisilicon Tech. Co., Ltd.
 ******************************************************************************
    Modification:  2015-1 Created
******************************************************************************/

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* End of #ifdef __cplusplus */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>     /* sockaddr_in{} and other Internet defns */
#include <net/route.h>
#include <arpa/inet.h>      /* inet(3) functions */
#include <net/if.h>

#include "sample_comm.h"

#define MAXDATA 512
#define HDMI_SUPPORT


#define true 1
#define false 0

int curt0Vi = 0;
int curt1Vi = 1;
int vpss0On = 0;
int vpss1On = 2;


static PAYLOAD_TYPE_E gs_enPayloadType = PT_AAC;
//static HI_BOOL gs_bMicIn = HI_FALSE;
static HI_BOOL gs_bAiVqe= HI_FALSE;
static HI_BOOL gs_bAioReSample = HI_FALSE;
static HI_BOOL gs_bUserGetMode = HI_FALSE;
static HI_BOOL gs_bAoVolumeCtrl = HI_FALSE;
static AUDIO_RESAMPLE_ATTR_S *gs_pstAiReSmpAttr = NULL;
static AUDIO_RESAMPLE_ATTR_S *gs_pstAoReSmpAttr = NULL;
static HI_U32 g_u32AiCnt = 0;
static HI_U32 g_u32AoCnt = 0;
static HI_U32 g_u32AencCnt = 0;
static HI_U32 g_u32Adec = 0;
static HI_U32 g_u32AiDev = 0;
static HI_U32 g_u32AoDev = 0;

void* udpServerProc(void *arg);
#define SAMPLE_DBG(s32Ret)\
do{\
    printf("s32Ret=%#x,fuc:%s,line:%d\n", s32Ret, __FUNCTION__, __LINE__);\
}while(0)
/******************************************************************************
* function : show usage
******************************************************************************/
void SAMPLE_VIO_Usage(char *sPrgNm)
{
    printf("Usage : %s + <index> (eg: %s 0)\n", sPrgNm,sPrgNm);
    printf("index:\n");
	printf("\t0:  VI 1080P input, HD0(VGA+BT1120),HD1(HDMI)/SD VO \n");
	printf("\t1:  VI 1080p input, HD ZoomIn\n");
	printf("\tq:  quit\n");

    return;
}

/******************************************************************************
* function : to process abnormal case
******************************************************************************/
void SAMPLE_VIO_HandleSig(HI_S32 signo)
{
    if (SIGINT == signo || SIGTERM == signo)
    {   SAMPLE_COMM_VO_HdmiStop();
        SAMPLE_COMM_SYS_Exit();
        printf("\033[0;31mprogram termination abnormally!\033[0;39m\n");
    }
    exit(-1);
}

/******************************************************************************
* function : Ai -> Ao(Hdmi)
******************************************************************************/
HI_S32 SAMPLE_AUDIO_AiCon4HdmiAo(HI_VOID)
{
    HI_S32 s32Ret, s32AiChnCnt;
    AUDIO_DEV   AiDev = 2;
    AI_CHN      AiChn = 0;
    AUDIO_DEV   AoDev = SAMPLE_AUDIO_HDMI_AO_DEV;
    AO_CHN      AoChn = 0;
	HI_U32 		i;

    AIO_ATTR_S stAioAttr;
	
    AIO_ATTR_S stHdmiAoAttr;
    AUDIO_RESAMPLE_ATTR_S stAoReSampleAttr;

    stAioAttr.enSamplerate   = AUDIO_SAMPLE_RATE_32000;
    stAioAttr.enBitwidth     = AUDIO_BIT_WIDTH_16;
    stAioAttr.enWorkmode     = AIO_MODE_I2S_MASTER;
    stAioAttr.enSoundmode    = AUDIO_SOUND_MODE_MONO;
    stAioAttr.u32EXFlag      = 1;
    stAioAttr.u32FrmNum      = 30;
    stAioAttr.u32PtNumPerFrm = SAMPLE_AUDIO_PTNUMPERFRM;
    stAioAttr.u32ChnCnt      = 2;
	stAioAttr.u32ClkChnCnt   = 2;
    stAioAttr.u32ClkSel      = 1;

    stHdmiAoAttr.enSamplerate   = AUDIO_SAMPLE_RATE_32000;
    stHdmiAoAttr.enBitwidth     = AUDIO_BIT_WIDTH_16;
    stHdmiAoAttr.enWorkmode     = AIO_MODE_I2S_MASTER;
    stHdmiAoAttr.enSoundmode    = AUDIO_SOUND_MODE_MONO;
    stHdmiAoAttr.u32EXFlag      = 1;
    stHdmiAoAttr.u32FrmNum      = 30;
    stHdmiAoAttr.u32PtNumPerFrm = SAMPLE_AUDIO_PTNUMPERFRM;
    stHdmiAoAttr.u32ChnCnt      = 2;
	stHdmiAoAttr.u32ClkChnCnt   = 2;
    stHdmiAoAttr.u32ClkSel      = 1;

    /* ao 8k -> 48k */
    stAoReSampleAttr.u32InPointNum  = SAMPLE_AUDIO_PTNUMPERFRM;
    stAoReSampleAttr.enInSampleRate = AUDIO_SAMPLE_RATE_16000;
    stAoReSampleAttr.enOutSampleRate = AUDIO_SAMPLE_RATE_48000;
    gs_pstAoReSmpAttr = &stAoReSampleAttr;
    gs_bAioReSample = HI_FALSE;
    /* resample and anr should be user get mode */
    gs_bUserGetMode = (HI_TRUE == gs_bAioReSample || HI_TRUE == gs_bAiVqe) ? HI_TRUE : HI_FALSE;

    /* config audio codec */
    s32Ret = SAMPLE_COMM_AUDIO_CfgAcodec(&stAioAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_DBG(s32Ret);
        return HI_FAILURE;
    }

    /* enable AI channle */
    s32AiChnCnt = stAioAttr.u32ChnCnt;
	g_u32AiCnt = s32AiChnCnt;
	g_u32AiDev = AiDev;
    s32Ret = SAMPLE_COMM_AUDIO_StartAi(AiDev, s32AiChnCnt, &stAioAttr, AUDIO_SAMPLE_RATE_BUTT, HI_FALSE, NULL, 0);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_DBG(s32Ret);
        return HI_FAILURE;
    }

    /* enable AO channle */
	g_u32AoCnt = stHdmiAoAttr.u32ChnCnt;
	g_u32AoDev = AoDev;
    s32Ret = SAMPLE_COMM_AUDIO_StartAo(AoDev, stHdmiAoAttr.u32ChnCnt, &stHdmiAoAttr, gs_pstAoReSmpAttr->enInSampleRate, HI_FALSE, NULL, 0);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_DBG(s32Ret);
        return HI_FAILURE;
    }

    /* AI to AO channel */
	for (i=0; i<g_u32AoCnt; i++)
	{
		AiChn = i;
		AoChn = i;
		#if 0
		s32Ret = SAMPLE_COMM_AUDIO_CreatTrdAiAo(AiDev, AiChn, AoDev, AoChn);
	    if (s32Ret != HI_SUCCESS)
	    {
	        SAMPLE_DBG(s32Ret);
	        return HI_FAILURE;
	    }
		#endif
		s32Ret = SAMPLE_COMM_AUDIO_AoBindAi(AiDev, i, AoDev, i);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_DBG(s32Ret);
            return HI_FAILURE;
        	}
	}
	return 0;
    printf("\nplease press twice ENTER to exit this sample\n");
    getchar();
    getchar();

	for (i=0; i<g_u32AoCnt; i++)
	{
		AiChn = i;
	    s32Ret = SAMPLE_COMM_AUDIO_DestoryTrdAi(AiDev, AiChn);
	    if (s32Ret != HI_SUCCESS)
	    {
	        SAMPLE_DBG(s32Ret);
	        return HI_FAILURE;
	    }
	}

    s32Ret = SAMPLE_COMM_AUDIO_StopAi(AiDev, s32AiChnCnt, HI_FALSE, HI_FALSE);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_DBG(s32Ret);
        return HI_FAILURE;
    }

    s32Ret = SAMPLE_COMM_AUDIO_StopAo(AoDev, stHdmiAoAttr.u32ChnCnt, HI_TRUE, HI_FALSE);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_DBG(s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 SAMPLE_AUDIO_TLV320AIC3101AiAo1(HI_VOID)
{
    HI_S32 s32Ret, s32AiChnCnt, s32AoChnCnt;
    AUDIO_DEV   AiDev = 2;   //con7 ai1 /con8 ai0
    AI_CHN      AiChn = 0;
    AUDIO_DEV   AoDev = 1;   //ao0 3106/ ao2 hdmi  /ao1 si9022
    AO_CHN      AoChn = 0;
    AIO_ATTR_S stAioAttr;
    AUDIO_RESAMPLE_ATTR_S stAiReSampleAttr;
    AUDIO_RESAMPLE_ATTR_S stAoReSampleAttr;
	int i;

    stAioAttr.enSamplerate   = AUDIO_SAMPLE_RATE_48000;
    stAioAttr.enBitwidth     = AUDIO_BIT_WIDTH_16;
    stAioAttr.enWorkmode     = AIO_MODE_I2S_MASTER;
    stAioAttr.enSoundmode    = AUDIO_SOUND_MODE_MONO;
    stAioAttr.u32EXFlag      = 1;
    stAioAttr.u32FrmNum      = 30;
    stAioAttr.u32PtNumPerFrm = SAMPLE_AUDIO_PTNUMPERFRM;
    stAioAttr.u32ChnCnt      = 2;
    stAioAttr.u32ClkChnCnt   = 2;
    stAioAttr.u32ClkSel      = 0;



	gs_bAioReSample = HI_FALSE;


    gs_pstAiReSmpAttr = NULL;
    gs_pstAoReSmpAttr = NULL;
    


    /* resample and anr should be user get mode */
    gs_bUserGetMode = (HI_TRUE == gs_bAioReSample || HI_TRUE == gs_bAiVqe) ? HI_TRUE : HI_FALSE;

    /* config audio codec */
    s32Ret = SAMPLE_COMM_AUDIO_CfgAcodec(&stAioAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_DBG(s32Ret);
        return HI_FAILURE;
    }

    /* enable AI channle */
    s32AiChnCnt = stAioAttr.u32ChnCnt >> stAioAttr.enSoundmode;
	g_u32AiCnt = s32AiChnCnt;
	g_u32AiDev = AiDev;
	stAioAttr.enWorkmode     = AIO_MODE_I2S_SLAVE;
    s32Ret = SAMPLE_COMM_AUDIO_StartAi(AiDev, s32AiChnCnt, &stAioAttr, (gs_pstAiReSmpAttr != NULL ? gs_pstAiReSmpAttr->enOutSampleRate : AUDIO_SAMPLE_RATE_BUTT),
        (gs_pstAiReSmpAttr != NULL ? HI_TRUE : HI_FALSE), NULL, 0);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_DBG(s32Ret);
        return HI_FAILURE;
    }

    /* enable AO channle */
    //stAioAttr.u32ChnCnt = 2;
    s32AoChnCnt = stAioAttr.u32ChnCnt >> stAioAttr.enSoundmode;
	g_u32AoCnt = s32AoChnCnt;
	g_u32AoDev = AoDev;
 	stAioAttr.enWorkmode     = AIO_MODE_I2S_MASTER;
    s32Ret = SAMPLE_COMM_AUDIO_StartAo(AoDev, s32AoChnCnt, &stAioAttr, (gs_pstAoReSmpAttr != NULL ? gs_pstAoReSmpAttr->enInSampleRate : AUDIO_SAMPLE_RATE_BUTT),
    (gs_pstAoReSmpAttr != NULL ? HI_TRUE : HI_FALSE), NULL, 0);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_DBG(s32Ret);
        return HI_FAILURE;
    }
	
	for (i = 0; i < s32AoChnCnt; i++){
        /* bind AI to AO channle */
        if (HI_TRUE == gs_bUserGetMode)
        {
            s32Ret = SAMPLE_COMM_AUDIO_CreatTrdAiAo(AiDev, AiChn, AoDev, AoChn);
             if (s32Ret != HI_SUCCESS)
             {
                 SAMPLE_DBG(s32Ret);
                 return HI_FAILURE;
        		 }
    		 }
    	else
    	{

      		s32Ret = SAMPLE_COMM_AUDIO_AoBindAi(AiDev, i, AoDev, i);
        	if (s32Ret != HI_SUCCESS)
           	{
               	SAMPLE_DBG(s32Ret);
               	return HI_FAILURE;
           	}
        }
    
    	printf("ai->ao : ai(%d,%d) bind to ao(%d,%d) ok\n", AiDev, i, AoDev, i);
	}

    if(HI_TRUE == gs_bAoVolumeCtrl)
    {
        s32Ret = SAMPLE_COMM_AUDIO_CreatTrdAoVolCtrl(AoDev);
        if(s32Ret != HI_SUCCESS)
        {
            SAMPLE_DBG(s32Ret);
            return HI_FAILURE;
        }
    }

    printf("\nplease press twice ENTER to exit this sample\n");
    getchar();
    getchar();

    if(HI_TRUE == gs_bAoVolumeCtrl)
    {
        s32Ret = SAMPLE_COMM_AUDIO_DestoryTrdAoVolCtrl(AoDev);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_DBG(s32Ret);
            return HI_FAILURE;
        }
    }
	for (i = 0; i < s32AoChnCnt; i++){

    	if (HI_TRUE == gs_bUserGetMode)
    	{
        	s32Ret = SAMPLE_COMM_AUDIO_DestoryTrdAi(AiDev, AiChn);
     	   if (s32Ret != HI_SUCCESS)
     	   {
     	       SAMPLE_DBG(s32Ret);
     	       return HI_FAILURE;
      	  }
    	}
    	else
   		{
 
           s32Ret = SAMPLE_COMM_AUDIO_AoUnbindAi(AiDev, i, AoDev, i);
           if (s32Ret != HI_SUCCESS)
           {
               SAMPLE_DBG(s32Ret);
               return HI_FAILURE;
           }
        }
        
    }

    s32Ret = SAMPLE_COMM_AUDIO_StopAi(AiDev, s32AiChnCnt, gs_bAioReSample, HI_FALSE);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_DBG(s32Ret);
        return HI_FAILURE;
    }

    s32Ret = SAMPLE_COMM_AUDIO_StopAo(AoDev, s32AoChnCnt, gs_bAioReSample, HI_FALSE);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_DBG(s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

int setnonblocking(int socket_)
{
    if (-1 == fcntl(socket_, F_SETFL, fcntl(socket_, F_GETFD, 0)|O_NONBLOCK))
        return false;

    return true;
}

int CreateUDPServer(const uint k_serverport,struct sockaddr_in *serveraddr_)
{
    int b_nonblocking = false;
    int b_readdr = true;
    int socket_ = 0;
    serveraddr_->sin_family = AF_INET;
    serveraddr_->sin_addr.s_addr = htonl (INADDR_ANY);
    serveraddr_->sin_port = htons(k_serverport);

    socket_ = socket(AF_INET, SOCK_DGRAM, 0);
    if (socket_ == -1) {
        perror("can't create socket file");
        return 0;
    }

    if(b_readdr){
        int opt = 1;
        if(0 > setsockopt(socket_, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))){
            perror("setsockopt():SO_REUSEADDR");
            return 0;
        }
    }

    if(b_nonblocking){
        if (setnonblocking(socket_) < 0){
            perror("setnonblock error");
            return 0;
        }
    }

    if (-1 == bind(socket_, (struct sockaddr *) serveraddr_, sizeof(struct sockaddr))){
        perror("bind error");
        return 0;
    }

    return socket_;
}


pthread_t CreateThread(void* (*pThreadFunc)(void*), uint priority, int schedpolicy, int b_detached, void *threadin)
{
    pthread_t tid;
    pthread_attr_t attr;
    struct sched_param  param;
    pthread_attr_init(&attr);
    param.sched_priority = priority; //优先级：1～99
    //pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);//要使优先级其作用必须要有这句话.表示使用在schedpolicy和schedparam属性中显式设置的调度策略和参数
    pthread_attr_setschedparam(&attr, &param);
    pthread_attr_setschedpolicy(&attr, schedpolicy);

    if(b_detached)
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED); //分离模式，无需的等待（pthread_join）之后才释放占用资源，而是返回后自动释放占用的资源

    if(pthread_create(&tid, &attr, pThreadFunc, threadin) < 0){
        return 0;
    }

    pthread_attr_destroy(&attr);

    //pthread_timedjoin_np(); //超时等待
    return tid;
}



long RecvFrom(char *p_message, long maxdatalen,int socket_)
{
    struct sockaddr_in clientaddr_;
    int socklen_ = sizeof(struct sockaddr_in);
    return recvfrom(socket_, p_message, maxdatalen, 0, (struct sockaddr*)&clientaddr_, &socklen_);
}

long SendTo(const uint k_clientport, const char *kp_clientip,const char *kp_message,  long maxdatalen,int socket_)
{

    struct sockaddr_in serveraddr;
    serveraddr.sin_family 		= AF_INET;
    serveraddr.sin_port   		= htons(k_clientport);
    serveraddr.sin_addr.s_addr 	= inet_addr(kp_clientip);

    return sendto(socket_, kp_message, maxdatalen, 0, (struct sockaddr*)&serveraddr, sizeof(serveraddr));
}

int GetInfo(const char *s_str,char *c_str,char *d_str)
{
    char str1 = '=';
    char str2 = 'O';
    char str3 = '=';
    char str4 = '\n';
    while( *s_str && (*s_str++ != str1));
    while(*s_str && (*s_str != str2))
        *c_str++ = *s_str++;
    *c_str = '\0';
    while(*s_str && (*s_str++ !=str3));
    while(*s_str && (*s_str !=str4))
        *d_str++ = *s_str++;
    *d_str ='\0';
    return 0;
}


void* udpServerProc(void *arg)
{
    printf("udpserver thread open\n");
    int  len = 0;
    uint port = *(uint*)arg;
    char data[MAXDATA]  = "";
    int socket_ = 0;
    char in[4];
    char out[4];
    int inNum = 0;
    int outNum = 0;
    HI_S32 s32Ret;
    struct sockaddr_in serveraddr_;



    socket_ = CreateUDPServer(port,&serveraddr_);
    if(socket_)
        printf("create udp server ok\n");
    else
        printf("create udp server error\n");

//    struct sockaddr_in clientaddr_;
//    int socklen_ = sizeof(struct sockaddr_in);
//    recvfrom(socket_, data, MAXDATA, 0, (struct sockaddr*)&clientaddr_, &socklen_);
//    printf("recv udp data: %s\n", data);

    while( 1 ){
        memset(data, 0, sizeof(data));
        printf("start recv data\n");
        if( (len = RecvFrom(data, MAXDATA,socket_)) > 0){
            printf("recv udp data: %s\n", data);
            if(!strcmp(data, "RSET:")){ //重启指令
                system("reboot");
                break;
            }
            else {
                memset(in,0,4);
                memset(out,0,4);
                if(0 == GetInfo(data,in,out)){
                    inNum = atoi(in);
                    outNum = atoi(out);
                    printf("vi-->%d  vo-->%d\n",inNum,outNum);
                    if(SAMPLE_VO_LAYER_VHD0 ==  outNum) {
                        if(curt0Vi == inNum) {
                            printf("no need change\n");
                        }
                        else {
                            if(HI_SUCCESS !=  (s32Ret = SAMPLE_COMM_VO_UnBindVpss(SAMPLE_VO_LAYER_VHD0,0,vpss0On,0)))
                            {
                               printf("vo unbind failed--->: %x\n", s32Ret);
                            }
                            sleep(3);
                            if(curt1Vi != inNum) {
                                vpss0On = 2*inNum;
                                if(HI_SUCCESS != (s32Ret = SAMPLE_COMM_VO_BindVpss(SAMPLE_VO_LAYER_VHD0,0,vpss0On,0)))
                                {
                                    printf("vo bind vpss failed--->%x\n",s32Ret);
                                }
                            }
                            else {
                                if(vpss1On%2) {
                                    vpss0On = 2*inNum;
                                    if(HI_SUCCESS != (s32Ret = SAMPLE_COMM_VO_BindVpss(SAMPLE_VO_LAYER_VHD0,0,vpss0On,0)))
                                    {
                                        printf("vo bind vpss failed--->%x\n",s32Ret);
                                    }
                                }
                                else {
                                    vpss0On = 2*inNum +1;
                                    if(HI_SUCCESS != (s32Ret = SAMPLE_COMM_VO_BindVpss(SAMPLE_VO_LAYER_VHD0,0,vpss0On,0)))
                                    {
                                        printf("vo bind vpss failed--->%x\n",s32Ret);
                                    }
                                }
                            }
                            curt0Vi = inNum;
                        }
                    }
                    else if(SAMPLE_VO_LAYER_VHD1 == outNum) {
                        if(curt1Vi == inNum) {
                            printf("no need change\n");
                        }
                        else {
                            if(HI_SUCCESS !=  (s32Ret = SAMPLE_COMM_VO_UnBindVpss(SAMPLE_VO_LAYER_VHD1,0,vpss1On,0)))
                            {
                               printf("vo unbind failed--->: %x\n", s32Ret);
                            }
                            sleep(2);
                            if(curt0Vi != inNum) {
                                vpss1On = 2*inNum;
                                if(HI_SUCCESS != (s32Ret = SAMPLE_COMM_VO_BindVpss(SAMPLE_VO_LAYER_VHD1,0,vpss1On,0)))
                                {
                                    printf("vo bind vpss failed--->%x\n",s32Ret);
                                }
                            }
                            else {
                                if(vpss0On%2) {
                                    vpss1On = 2*inNum;
                                    if(HI_SUCCESS != (s32Ret = SAMPLE_COMM_VO_BindVpss(SAMPLE_VO_LAYER_VHD1,0,vpss1On,0)))
                                    {
                                        printf("vo bind vpss failed--->%x\n",s32Ret);
                                    }
                                }
                                else {
                                    vpss1On = 2*inNum +1;
                                    if(HI_SUCCESS != (s32Ret = SAMPLE_COMM_VO_BindVpss(SAMPLE_VO_LAYER_VHD1,0,vpss1On,0)))
                                    {
                                        printf("vo bind vpss failed--->%x\n",s32Ret);
                                    }
                                }
                            }
                            curt1Vi = inNum;
                        }
                    }
                    else
                        printf("error Num\n");
                }
                else
                    SAMPLE_PRT("data error\n");
            }
        }
        else
            break;
    }
    return (void*)0;
}


/******************************************************************************
* function : VI(1080P: 8 windows) -> VPSS -> HD0(1080P,VGA,BT1120)
                                         -> HD1(1080P,HDMI)
                                         -> SD0(D1)
******************************************************************************/
HI_S32 SAMPLE_VIO_8_1080P(HI_VOID)
{
	SAMPLE_VI_MODE_E enViMode = SAMPLE_VI_MODE_4_1080P;
	VIDEO_NORM_E enNorm = VIDEO_ENCODING_MODE_NTSC;

	HI_U32 u32ViChnCnt = 8;
        HI_S32 s32VpssGrpCnt = 8;

	VB_CONF_S stVbConf;
	VPSS_GRP VpssGrp;
	VPSS_GRP_ATTR_S stGrpAttr;
        VPSS_CHN VpssChn_VoHD0 = VPSS_CHN1;
	VPSS_CHN VpssChn_VoHD1 = VPSS_CHN2;
	VPSS_CHN VpssChn_VoSD0 = VPSS_CHN3;

	VO_DEV VoDev;
	VO_LAYER VoLayer;
	VO_CHN VoChn;
	VO_PUB_ATTR_S stVoPubAttr_hd0, stVoPubAttr_hd1, stVoPubAttr_sd;
	VO_VIDEO_LAYER_ATTR_S stLayerAttr;
	SAMPLE_VO_MODE_E enVoMode, enPreVoMode;

	HI_S32 i;
	HI_S32 s32Ret = HI_SUCCESS;
	HI_U32 u32BlkSize;
	HI_S32 ch;
	SIZE_S stSize;
	HI_U32 u32WndNum;
	HI_U32 vichn;

        HI_U32 udpPort = 41234;
        int b_detached = true;
        pthread_t udp_tid = CreateThread(udpServerProc, 0, SCHED_FIFO, b_detached, &udpPort);
        if(udp_tid == 0){
            SAMPLE_PRT("pthread_create() failed: net udpBroadCastProc");
        }

	/******************************************
	 step  1: init variable
	******************************************/
	memset(&stVbConf,0,sizeof(VB_CONF_S));
	u32BlkSize = SAMPLE_COMM_SYS_CalcPicVbBlkSize(enNorm,\
				PIC_HD1080, SAMPLE_PIXEL_FORMAT, SAMPLE_SYS_ALIGN_WIDTH,COMPRESS_MODE_SEG);
	stVbConf.u32MaxPoolCnt = 128;

	/* video buffer*/
	//todo: vb=15
	stVbConf.astCommPool[0].u32BlkSize = u32BlkSize;
	stVbConf.astCommPool[0].u32BlkCnt = u32ViChnCnt * 8;

	/******************************************
	 step 2: mpp system init.
	******************************************/
	s32Ret = SAMPLE_COMM_SYS_Init(&stVbConf);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("system init failed with %d!\n", s32Ret);
		goto END_8_1080P_0;
	}

	/******************************************
         *
         *
	 step 3: start vi dev & chn
	******************************************/
	s32Ret = SAMPLE_COMM_VI_Start(enViMode, enNorm);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("start vi failed!\n");
		goto END_8_1080P_0;
	}


	/******************************************
	 step 4: start vpss and vi bind vpss
	******************************************/
	s32Ret = SAMPLE_COMM_SYS_GetPicSize(enNorm, PIC_HD1080, &stSize);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("SAMPLE_COMM_SYS_GetPicSize failed!\n");
		goto END_8_1080P_1;
	}

	memset(&stGrpAttr,0,sizeof(VPSS_GRP_ATTR_S));
	stGrpAttr.u32MaxW = stSize.u32Width;
	stGrpAttr.u32MaxH = stSize.u32Height;
	stGrpAttr.bNrEn = HI_TRUE;
    stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
	stGrpAttr.enPixFmt = SAMPLE_PIXEL_FORMAT;
        s32Ret = SAMPLE_COMM_VPSS_Start(s32VpssGrpCnt, &stSize, 1, &stGrpAttr);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start Vpss failed!\n");
		goto END_8_1080P_1;
	}

	s32Ret = SAMPLE_COMM_VI_BindVpss(enViMode);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Vi bind Vpss failed!\n");
		goto END_8_1080P_2;
	}
#if 1
	/******************************************
	 step 5: start vo HD0 (bt1120+VGA), multi-screen, you can switch mode
	******************************************/
	printf("start vo HD0.\n");
	VoDev = SAMPLE_VO_DEV_DHD0;
	VoLayer = SAMPLE_VO_LAYER_VHD0;
        u32WndNum = 1;
        enVoMode = VO_MODE_1MUX;

	stVoPubAttr_hd0.enIntfSync = VO_OUTPUT_1080P60;
        stVoPubAttr_hd0.enIntfType = VO_INTF_VGA | VO_INTF_BT1120;
//        stVoPubAttr_hd0.enIntfType = VO_INTF_HDMI ;
	stVoPubAttr_hd0.u32BgColor = 0x003355;
	s32Ret = SAMPLE_COMM_VO_StartDev(VoDev, &stVoPubAttr_hd0);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartDev failed!\n");
		goto END_8_1080P_3;
	}

	memset(&(stLayerAttr), 0 , sizeof(VO_VIDEO_LAYER_ATTR_S));
	s32Ret = SAMPLE_COMM_VO_GetWH(stVoPubAttr_hd0.enIntfSync, \
		&stLayerAttr.stImageSize.u32Width, \
		&stLayerAttr.stImageSize.u32Height, \
		&stLayerAttr.u32DispFrmRt);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_GetWH failed!\n");
		goto END_8_1080P_4;
	}

	stLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
    stLayerAttr.stDispRect.s32X       = 0;
    stLayerAttr.stDispRect.s32Y       = 0;
    stLayerAttr.stDispRect.u32Width   = stLayerAttr.stImageSize.u32Width;
    stLayerAttr.stDispRect.u32Height  = stLayerAttr.stImageSize.u32Height;
	s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayer, &stLayerAttr);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartLayer failed!\n");
		goto END_8_1080P_4;
	}

	s32Ret = SAMPLE_COMM_VO_StartChn(VoLayer, enVoMode);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartChn failed!\n");
		goto END_8_1080P_5;
	}
#ifdef HDMI_SUPPORT
	/* if it's displayed on HDMI, we should start HDMI */
	if (stVoPubAttr_hd0.enIntfType & VO_INTF_HDMI)
	{
		if (HI_SUCCESS != SAMPLE_COMM_VO_HdmiStart(stVoPubAttr_hd0.enIntfSync))
		{
			SAMPLE_PRT("Start SAMPLE_COMM_VO_HdmiStart failed!\n");
			goto END_8_1080P_5;
		}
	}
#endif
//        for(i=0;i<u32WndNum;i++)
//        {
//                VoChn = i;
//                VpssGrp = i;

//                s32Ret = SAMPLE_COMM_VO_BindVpss(VoDev,VoChn,VpssGrp,VpssChn_VoHD0);
//                if (HI_SUCCESS != s32Ret)
//                {
//                        SAMPLE_PRT("Start VO failed!\n");
//                        goto END_8_1080P_5;
//                }
//        }
        VoChn = 0;
        s32Ret = SAMPLE_COMM_VO_BindVpss(VoDev,VoChn,vpss0On,0);
        if (HI_SUCCESS != s32Ret)
        {
                SAMPLE_PRT("Start VO failed!\n");
                goto END_8_1080P_5;
        }
#endif

#if 1
    /******************************************
	 step 5: start vo HD1 (HDMI), multi-screen, you can switch mode
	******************************************/
	printf("start vo HD1.\n");
	VoDev = SAMPLE_VO_DEV_DHD1;
	VoLayer = SAMPLE_VO_LAYER_VHD1;
        u32WndNum = 1;
        enVoMode = VO_MODE_1MUX;

	stVoPubAttr_hd1.enIntfSync = VO_OUTPUT_1080P60;
//	stVoPubAttr_hd1.enIntfType = VO_INTF_HDMI | VO_INTF_BT1120; //
        stVoPubAttr_hd1.enIntfType = VO_INTF_HDMI;
	stVoPubAttr_hd1.u32BgColor = 0x003355;
	s32Ret = SAMPLE_COMM_VO_StartDev(VoDev, &stVoPubAttr_hd1);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartDev failed!\n");
		goto END_8_1080P_5;
	}

	memset(&(stLayerAttr), 0 , sizeof(VO_VIDEO_LAYER_ATTR_S));
	s32Ret = SAMPLE_COMM_VO_GetWH(stVoPubAttr_hd1.enIntfSync, \
		&stLayerAttr.stImageSize.u32Width, \
		&stLayerAttr.stImageSize.u32Height, \
		&stLayerAttr.u32DispFrmRt);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_GetWH failed!\n");
		goto END_8_1080P_6;
	}

	stLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
    stLayerAttr.stDispRect.s32X       = 0;
    stLayerAttr.stDispRect.s32Y       = 0;
    stLayerAttr.stDispRect.u32Width   = stLayerAttr.stImageSize.u32Width;
    stLayerAttr.stDispRect.u32Height  = stLayerAttr.stImageSize.u32Height;
	s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayer, &stLayerAttr);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartLayer failed!\n");
		goto END_8_1080P_6;
	}

	s32Ret = SAMPLE_COMM_VO_StartChn(VoLayer, enVoMode);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartChn failed!\n");
		goto END_8_1080P_7;
	}
#ifdef HDMI_SUPPORT
	/* if it's displayed on HDMI, we should start HDMI */
	if (stVoPubAttr_hd1.enIntfType & VO_INTF_HDMI)
	{
		if (HI_SUCCESS != SAMPLE_COMM_VO_HdmiStart(stVoPubAttr_hd1.enIntfSync))
		{
			SAMPLE_PRT("Start SAMPLE_COMM_VO_HdmiStart failed!\n");
			goto END_8_1080P_7;
		}
	}
#endif
//	for(i=0;i<u32WndNum;i++)
//	{
//		VoChn = i;
//		VpssGrp = i;

//		s32Ret = SAMPLE_COMM_VO_BindVpss(VoDev,VoChn,VpssGrp,VpssChn_VoHD1);
//		if (HI_SUCCESS != s32Ret)
//		{
//			SAMPLE_PRT("Start VO failed!\n");
//			goto END_8_1080P_7;
//		}
//	}
        VoChn = 0;
        s32Ret = SAMPLE_COMM_VO_BindVpss(VoDev,VoChn,vpss1On,0);
        if (HI_SUCCESS != s32Ret)
        {
                SAMPLE_PRT("Start VO failed!\n");
                goto END_8_1080P_5;
        }
	/*for(i=0;i<u32WndNum;i++)
	{
		VoChn = i;
		VpssGrp = i;

		s32Ret = SAMPLE_COMM_VO_BindVpss(SAMPLE_VO_DEV_DHD1,VoChn,VpssGrp,VpssChn_VoHD1);
		if (HI_SUCCESS != s32Ret)
		{
			SAMPLE_PRT("Start VO failed!\n");
			goto END_8_1080P_7;
		}
	}*/
//	SAMPLE_AUDIO_AiCon4HdmiAo();
#endif
#if 0
	/******************************************
	step 6: start vo SD0 (CVBS)
	******************************************/
	printf("start vo SD0\n");
	VoDev = SAMPLE_VO_DEV_DSD0;
	VoLayer = SAMPLE_VO_LAYER_VSD0;
	u32WndNum = 4;
	enVoMode = VO_MODE_4MUX;

	stVoPubAttr_sd.enIntfSync = VO_OUTPUT_PAL;
	stVoPubAttr_sd.enIntfType = VO_INTF_CVBS;
	stVoPubAttr_sd.u32BgColor = 0x0;
	s32Ret = SAMPLE_COMM_VO_StartDev(VoDev, &stVoPubAttr_sd);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartDev failed!\n");
		goto END_8_1080P_7;
	}

	memset(&(stLayerAttr), 0 , sizeof(VO_VIDEO_LAYER_ATTR_S));
	s32Ret = SAMPLE_COMM_VO_GetWH(stVoPubAttr_sd.enIntfSync, \
		&stLayerAttr.stImageSize.u32Width, \
		&stLayerAttr.stImageSize.u32Height, \
		&stLayerAttr.u32DispFrmRt);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_GetWH failed!\n");
		goto END_8_1080P_8;
	}

	stLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
    stLayerAttr.stDispRect.s32X       = 0;
    stLayerAttr.stDispRect.s32Y       = 0;
    stLayerAttr.stDispRect.u32Width   = stLayerAttr.stImageSize.u32Width;
    stLayerAttr.stDispRect.u32Height  = stLayerAttr.stImageSize.u32Height;
	s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayer, &stLayerAttr);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartLayer failed!\n");
		goto END_8_1080P_8;
	}

	s32Ret = SAMPLE_COMM_VO_StartChn(VoLayer, enVoMode);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartChn failed!\n");
		goto END_8_1080P_9;
	}

	for(i=0;i<u32WndNum;i++)
	{
		VoChn = i;
		VpssGrp = i;
		vichn = 4*i;

		s32Ret = SAMPLE_COMM_VO_BindVi(VoLayer,VoChn,vichn);
		if (HI_SUCCESS != s32Ret)
		{
			SAMPLE_PRT("Start VO failed!\n");
			goto END_8_1080P_9;
		}
	}
#endif

#if 0
	/******************************************
	step 7: HD0 switch mode
	******************************************/
	VoDev = SAMPLE_VO_DEV_DHD0;
	VoLayer = SAMPLE_VO_LAYER_VHD0;
	enVoMode = VO_MODE_9MUX;
	while(1)
	{
		enPreVoMode = enVoMode;

		printf("please choose preview mode, press 'q' to exit this sample.\n");
		printf("\t0) 1 preview\n");
		printf("\t1) 4 preview\n");
		printf("\t2) 8 preview\n");
		printf("\tq) quit\n");

		ch = getchar();
        if (10 == ch)
        {
            continue;
        }
		getchar();
		if ('0' == ch)
		{
			u32WndNum = 1;
			enVoMode = VO_MODE_1MUX;
		}
		else if ('1' == ch)
		{
			u32WndNum = 4;
			enVoMode = VO_MODE_4MUX;
		}
		/*Indeed only 8 chns show*/
		else if ('2' == ch)
		{
			u32WndNum = 9;
			enVoMode = VO_MODE_9MUX;
		}
		else if ('q' == ch)
		{
			break;
		}
		else
		{
			SAMPLE_PRT("preview mode invaild! please try again.\n");
			continue;
		}
		SAMPLE_PRT("vo(%d) switch to %d mode\n", VoDev, u32WndNum);

		s32Ret= HI_MPI_VO_SetAttrBegin(VoLayer);
		if (HI_SUCCESS != s32Ret)
		{
			SAMPLE_PRT("Start VO failed!\n");
			goto END_8_1080P_9;
		}

		s32Ret = SAMPLE_COMM_VO_StopChn(VoLayer, enPreVoMode);
		if (HI_SUCCESS != s32Ret)
		{
			SAMPLE_PRT("Start VO failed!\n");
			goto END_8_1080P_9;
		}

		s32Ret = SAMPLE_COMM_VO_StartChn(VoLayer, enVoMode);
		if (HI_SUCCESS != s32Ret)
		{
			SAMPLE_PRT("Start VO failed!\n");
			goto END_8_1080P_9;
		}
		s32Ret= HI_MPI_VO_SetAttrEnd(VoLayer);
		if (HI_SUCCESS != s32Ret)
		{
			SAMPLE_PRT("Start VO failed!\n");
			goto END_8_1080P_9;
		}
	}
#endif
        while(1) {
            ch = getchar();
            if('q' ==  ch)
                break;
        }
	/******************************************
	 step 8: exit process
	******************************************/
END_8_1080P_9:
	VoDev = SAMPLE_VO_DEV_DSD0;
	VoLayer = SAMPLE_VO_LAYER_VSD0;
	u32WndNum = 4;
	enVoMode = VO_MODE_4MUX;
	SAMPLE_COMM_VO_StopChn(VoLayer, enVoMode);
	for(i=0;i<u32WndNum;i++)
	{
		VoChn = i;
		VpssGrp = i;
		SAMPLE_COMM_VO_UnBindVpss(VoLayer,VoChn,VpssGrp,VpssChn_VoSD0);
	}
	SAMPLE_COMM_VO_StopLayer(VoLayer);
END_8_1080P_8:

	SAMPLE_COMM_VO_StopDev(VoDev);

END_8_1080P_7:
	#ifdef HDMI_SUPPORT
	if (stVoPubAttr_hd1.enIntfType & VO_INTF_HDMI)
	{
		SAMPLE_COMM_VO_HdmiStop();
	}
	#endif
	VoDev = SAMPLE_VO_DEV_DHD1;
	VoLayer = SAMPLE_VO_LAYER_VHD1;
	u32WndNum = 4;
	enVoMode = VO_MODE_4MUX;
	SAMPLE_COMM_VO_StopChn(VoLayer, enVoMode);
	for(i=0;i<u32WndNum;i++)
	{
		VoChn = i;
		VpssGrp = i;
		SAMPLE_COMM_VO_UnBindVpss(VoLayer,VoChn,VpssGrp,VpssChn_VoHD1);
	}
	SAMPLE_COMM_VO_StopLayer(VoLayer);

END_8_1080P_6:
	SAMPLE_COMM_VO_StopDev(VoDev);

END_8_1080P_5:

	#ifdef HDMI_SUPPORT
	if (stVoPubAttr_hd0.enIntfType & VO_INTF_HDMI)
	{
		SAMPLE_COMM_VO_HdmiStop();
	}
	#endif

    VoDev = SAMPLE_VO_DEV_DHD0;
	VoLayer = SAMPLE_VO_LAYER_VHD0;
	u32WndNum = 4;
	enVoMode = VO_MODE_4MUX;
	SAMPLE_COMM_VO_StopChn(VoLayer, enVoMode);
	for(i=0;i<u32WndNum;i++)
	{
		VoChn = i;
		VpssGrp = i;
		SAMPLE_COMM_VO_UnBindVpss(VoLayer,VoChn,VpssGrp,VpssChn_VoHD0);
	}
	SAMPLE_COMM_VO_StopLayer(VoLayer);
END_8_1080P_4:
	SAMPLE_COMM_VO_StopDev(VoDev);
END_8_1080P_3:	//vi unbind vpss
	SAMPLE_COMM_VI_UnBindVpss(enViMode);
END_8_1080P_2:	//vpss stop
	SAMPLE_COMM_VPSS_Stop(s32VpssGrpCnt, VPSS_MAX_CHN_NUM);
END_8_1080P_1:	//vi stop
	SAMPLE_COMM_VI_Stop(enViMode);
END_8_1080P_0:	//system exit
	SAMPLE_COMM_SYS_Exit();

	return s32Ret;
}

/******************************************************************************
* function : VI(D1) -> VPSS grp 0 -> VO HD0(1080p)
*            VI(D1) -> VPSS grp 1 -> VO PIP
******************************************************************************/
HI_S32 SAMPLE_VIO_HD_ZoomIn()
{
    SAMPLE_VI_MODE_E enViMode = SAMPLE_VI_MODE_8_1080P;
	VIDEO_NORM_E enNorm = VIDEO_ENCODING_MODE_NTSC;

    HI_U32 u32ViChnCnt = 8;
    HI_S32 s32VpssGrpCnt = 8;
    VPSS_GRP VpssGrp = 0;
    VPSS_GRP VpssGrp_Clip = 1;
	VPSS_CHN VpssChn_VoHD0 = VPSS_CHN1;
	VPSS_CHN VpssChn_VoPIP = VPSS_CHN2;
    VO_DEV VoDev = SAMPLE_VO_DEV_DHD0;
	VO_LAYER VoLayer = SAMPLE_VO_LAYER_VHD0;
	VO_LAYER VoLayerPip = SAMPLE_VO_LAYER_VPIP;
    VO_CHN VoChn = 0;

    VB_CONF_S stVbConf;
    VPSS_GRP_ATTR_S stGrpAttr;
    VO_PUB_ATTR_S stVoPubAttr;
    SAMPLE_VO_MODE_E enVoMode;
    VPSS_CROP_INFO_S stVpssCropInfo;
	VO_VIDEO_LAYER_ATTR_S stLayerAttr;
    SIZE_S stSize;

    HI_S32 i;
    HI_S32 s32Ret = HI_SUCCESS;
    HI_U32 u32BlkSize;
    HI_S32 ch;
    HI_U32 u32WndNum;

    /******************************************
     step  1: init variable
    ******************************************/
    memset(&stVbConf,0,sizeof(VB_CONF_S));
    u32BlkSize = SAMPLE_COMM_SYS_CalcPicVbBlkSize(enNorm,\
                PIC_HD1080, SAMPLE_PIXEL_FORMAT, SAMPLE_SYS_ALIGN_WIDTH,COMPRESS_MODE_SEG);
    stVbConf.u32MaxPoolCnt = 128;

    /* video buffer*/
    //todo: vb=15
    stVbConf.astCommPool[0].u32BlkSize = u32BlkSize;
    stVbConf.astCommPool[0].u32BlkCnt = u32ViChnCnt * 8;

    /******************************************
     step 2: mpp system init.
    ******************************************/
    s32Ret = SAMPLE_COMM_SYS_Init(&stVbConf);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("system init failed with %d!\n", s32Ret);
        goto END_HDZOOMIN_0;
    }

    /******************************************
     step 3: start vi dev & chn
    ******************************************/
    s32Ret = SAMPLE_COMM_VI_Start(enViMode, enNorm);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("start vi failed!\n");
        goto END_HDZOOMIN_0;
    }

    /******************************************
     step 4: start vpss and vi bind vpss
    ******************************************/
    s32Ret = SAMPLE_COMM_SYS_GetPicSize(enNorm, PIC_HD1080, &stSize);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("SAMPLE_COMM_SYS_GetPicSize failed!\n");
        goto END_HDZOOMIN_0;
    }

	memset(&stGrpAttr,0,sizeof(VPSS_GRP_ATTR_S));
	stGrpAttr.u32MaxW = stSize.u32Width;
	stGrpAttr.u32MaxH = stSize.u32Height;
	stGrpAttr.bNrEn = HI_TRUE;
    stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
	stGrpAttr.enPixFmt = SAMPLE_PIXEL_FORMAT;
    s32Ret = SAMPLE_COMM_VPSS_Start(s32VpssGrpCnt, &stSize, VPSS_MAX_CHN_NUM,NULL);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start Vpss failed!\n");
        goto END_HDZOOMIN_1;
    }

    s32Ret = SAMPLE_COMM_VI_BindVpss(enViMode);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Vi bind Vpss failed!\n");
        goto END_HDZOOMIN_2;
    }

    /******************************************
     step 5: start VO to preview
    ******************************************/
	printf("start vo HD0.\n");
	VoDev = SAMPLE_VO_DEV_DHD0;
	VoLayer = SAMPLE_VO_LAYER_VHD0;
	u32WndNum = 1;
	enVoMode = VO_MODE_1MUX;

	stVoPubAttr.enIntfSync = VO_OUTPUT_1080P60;
	stVoPubAttr.enIntfType = VO_INTF_HDMI|VO_INTF_VGA;
	stVoPubAttr.u32BgColor = 0x000000ff;
	s32Ret = SAMPLE_COMM_VO_StartDev(VoDev, &stVoPubAttr);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartDev failed!\n");
		goto END_HDZOOMIN_3;
	}

	memset(&(stLayerAttr), 0 , sizeof(VO_VIDEO_LAYER_ATTR_S));
	s32Ret = SAMPLE_COMM_VO_GetWH(stVoPubAttr.enIntfSync, \
		&stLayerAttr.stImageSize.u32Width, \
		&stLayerAttr.stImageSize.u32Height, \
		&stLayerAttr.u32DispFrmRt);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_GetWH failed!\n");
		goto END_HDZOOMIN_3;
	}
	stLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
	stLayerAttr.stDispRect.s32X 	  = 0;
	stLayerAttr.stDispRect.s32Y 	  = 0;
	stLayerAttr.stDispRect.u32Width   = stLayerAttr.stImageSize.u32Width;
	stLayerAttr.stDispRect.u32Height  = stLayerAttr.stImageSize.u32Height;
	s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayer, &stLayerAttr);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartLayer failed!\n");
		goto END_HDZOOMIN_3;
	}

	s32Ret = SAMPLE_COMM_VO_StartChn(VoLayer, enVoMode);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start SAMPLE_COMM_VO_StartChn failed!\n");
		goto END_HDZOOMIN_4;
	}

#ifdef HDMI_SUPPORT
	/* if it's displayed on HDMI, we should start HDMI */
	if (stVoPubAttr.enIntfType & VO_INTF_HDMI)
	{
		if (HI_SUCCESS != SAMPLE_COMM_VO_HdmiStart(stVoPubAttr.enIntfSync))
		{
			SAMPLE_PRT("Start SAMPLE_COMM_VO_HdmiStart failed!\n");
			goto END_HDZOOMIN_5;
		}
	}
#endif

	VoChn = 0;
	VpssGrp = 0;
	s32Ret = SAMPLE_COMM_VO_BindVpss(VoDev,VoChn,VpssGrp,VpssChn_VoHD0);
	if (HI_SUCCESS != s32Ret)
	{
		SAMPLE_PRT("Start VO failed!\n");
		goto END_HDZOOMIN_5;
	}

    /******************************************
     step 6: Clip process
    ******************************************/
    printf("press any key to show hd zoom.\n");
    getchar();
    /*** enable vo pip layer ***/
    stLayerAttr.bClusterMode = HI_TRUE;
    stLayerAttr.bDoubleFrame = HI_FALSE;
    stLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
    //default, PIP bind dev0
    HI_MPI_VO_UnBindVideoLayer(VoLayerPip, 0);
	s32Ret = HI_MPI_VO_BindVideoLayer(VoLayerPip, VoDev);
	if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VO_BindVideoLayer failed with %#x!\n", s32Ret);
        goto END_HDZOOMIN_5;
    }

	memset(&stLayerAttr, 0 , sizeof(VO_VIDEO_LAYER_ATTR_S));
    s32Ret = SAMPLE_COMM_VO_GetWH(VO_OUTPUT_PAL, \
        &stLayerAttr.stDispRect.u32Width, \
        &stLayerAttr.stDispRect.u32Height, \
        &stLayerAttr.u32DispFrmRt);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        goto  END_HDZOOMIN_6;
    }
    stLayerAttr.stImageSize.u32Width = stLayerAttr.stDispRect.u32Width;
    stLayerAttr.stImageSize.u32Height = stLayerAttr.stDispRect.u32Height;
	stLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
    s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayerPip, &stLayerAttr);
    if (HI_SUCCESS != s32Ret)
    {
       SAMPLE_PRT("SAMPLE_COMM_VO_StartLayer failed!\n");
       goto END_HDZOOMIN_6;
    }

	enVoMode = VO_MODE_1MUX;
    s32Ret = SAMPLE_COMM_VO_StartChn(VoLayerPip,enVoMode);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        goto  END_HDZOOMIN_7;
    }

	s32Ret = SAMPLE_COMM_VO_BindVpss(VoLayerPip, VoChn, VpssGrp_Clip, VpssChn_VoPIP);//VpssGrp_Clip
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("SAMPLE_COMM_VO_BindVpss failed!\n");
        goto END_HDZOOMIN_8;
    }

	/*** enable vpss group clip ***/
    stVpssCropInfo.bEnable = HI_TRUE;
    stVpssCropInfo.enCropCoordinate = VPSS_CROP_ABS_COOR;
    stVpssCropInfo.stCropRect.s32X = 0;
    stVpssCropInfo.stCropRect.s32Y = 0;
    stVpssCropInfo.stCropRect.u32Height = 400;
    stVpssCropInfo.stCropRect.u32Width = 400;
    s32Ret = HI_MPI_VPSS_SetGrpCrop(VpssGrp_Clip, &stVpssCropInfo);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VPSS_SetGrpCrop failed with %#x!\n", s32Ret);
        goto END_HDZOOMIN_8;
    }

    printf("\npress 'q' to stop sample ... ... \n");
    while('q' != (ch = getchar()) )  {}
    goto END_HDZOOMIN_9;
    /******************************************
     step 7: exit process
    ******************************************/

END_HDZOOMIN_9:
	SAMPLE_COMM_VO_UnBindVpss(SAMPLE_VO_LAYER_VHD0, VoChn, VpssGrp_Clip, VpssChn_VoPIP);
END_HDZOOMIN_8:
	enVoMode = VO_MODE_1MUX;
	SAMPLE_COMM_VO_StopChn(VoLayerPip,enVoMode);
END_HDZOOMIN_7:
	SAMPLE_COMM_VO_StopLayer(VoLayerPip);
END_HDZOOMIN_6:
	HI_MPI_VO_UnBindVideoLayer(VoLayerPip,SAMPLE_VO_DEV_DHD0);
END_HDZOOMIN_5:
	VoDev = SAMPLE_VO_DEV_DHD0;
	VoLayer = SAMPLE_VO_LAYER_VHD0;
    u32WndNum = 1;
    enVoMode = VO_MODE_1MUX;
	for(i=0;i<u32WndNum;i++)
    {
        VoChn = i;
        VpssGrp = i;
        SAMPLE_COMM_VO_UnBindVpss(VoDev,VoChn,VpssGrp,VpssChn_VoHD0);
    }
    SAMPLE_COMM_VO_StopChn(VoDev, enVoMode);
END_HDZOOMIN_4:
	SAMPLE_COMM_VO_StopLayer(SAMPLE_VO_LAYER_VHD0);
	#ifdef HDMI_SUPPORT
    if (stVoPubAttr.enIntfType & VO_INTF_HDMI)
    {
        SAMPLE_COMM_VO_HdmiStop();
    }
	#endif
	SAMPLE_COMM_VO_StopDev(VoDev);
END_HDZOOMIN_3:
    SAMPLE_COMM_VI_UnBindVpss(enViMode);
END_HDZOOMIN_2:
    SAMPLE_COMM_VPSS_Stop(s32VpssGrpCnt, VPSS_MAX_CHN_NUM);
END_HDZOOMIN_1:
    SAMPLE_COMM_VI_Stop(enViMode);
END_HDZOOMIN_0:
    SAMPLE_COMM_SYS_Exit();

    return s32Ret;
}


/******************************************************************************
* function    : main()
* Description : video preview sample
******************************************************************************/
int main(int argc, char *argv[])
{
    HI_S32 s32Ret = HI_FAILURE;

    if ( (argc < 2) || (1 != strlen(argv[1])))
    {
        SAMPLE_VIO_Usage(argv[0]);
        return HI_FAILURE;
    }
    signal(SIGINT, SAMPLE_VIO_HandleSig);
    signal(SIGTERM, SAMPLE_VIO_HandleSig);

	switch(*argv[1])
	{
		case '0':/* VI:8*1080P;VPSS VO:HD0(HDMI|VGA); VPSS VO:SD0(CVBS)*/
			s32Ret = SAMPLE_VIO_8_1080P();
			break;
		case '1':/* VI:1*1080 HD PIP ZoomIn*/
			s32Ret = SAMPLE_VIO_HD_ZoomIn();
			break;
        default:
            printf("input invaild! please try again.\n");
            SAMPLE_VIO_Usage(argv[0]);
            return HI_FAILURE;
	}

    if (HI_SUCCESS == s32Ret)
        printf("program exit normally!\n");
    else
        printf("program exit abnormally!\n");
    exit(s32Ret);
}

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */

