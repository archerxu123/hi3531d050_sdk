#ifndef __GPIO_BASE_DRV_H__
#define __GPIO_BASE_DRV_H__

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

/************* gpio mux ctrls************/

int ema_gpio_is_valid(int number);

/* set as input or output, returning 0 or negative errno */
int ema_gpio_direction_input(unsigned int gpio);
int ema_gpio_direction_output(unsigned int gpio, int value);

/* GPIO INPUT: return zero or nonzero */
int ema_gpio_get_value(unsigned int gpio);

/* GPIO OUTPUT */
void ema_gpio_set_value(unsigned int gpio, int value);

int ema_gpio_cansleep(unsigned int gpio);


/* GPIO INPUT: return zero or nonzero, might sleep */
int ema_gpio_get_value_cansleep(unsigned int gpio);

/* GPIO OUTPUT, might sleep */
void ema_gpio_set_value_cansleep(unsigned int gpio, int value);


/* request GPIO, returning 0 or negative errno.
* non-null labels may be useful for diagnostics.
*/
int ema_gpio_request(unsigned int gpio, const char *label);

/* release previously-claimed GPIO */
void ema_gpio_free(unsigned int gpio);


/* map GPIO numbers to IRQ numbers */
int ema_gpio_to_irq(unsigned int gpio);

/* map IRQ numbers to GPIO numbers (avoid using this) */
//int ema_irq_to_gpio(unsigned irq);


int ema_get_irq_status(unsigned int gpio);


void ema_clear_irq_status(unsigned int gpio);



/* --------------------END ------------------------------*/
#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */

#endif	/* __GPIO_DRV_H__ */
